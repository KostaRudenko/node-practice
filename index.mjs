import utils from 'util';
import * as utilsOne from 'test-utils';

const params = { password: 'sdf234fdsf32cdsc' };

const toPromis = utils.promisify(utilsOne.runMePlease);

try {
    const result = await toPromis(params);
    console.log(result);
} catch (error) {
    console.log(error)
}
